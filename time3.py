import websocket
import requests
from json import loads
import time
import ast

class Client():
    def __init__(self):
        # create websocket connection

        self.ws = websocket.WebSocketApp(
            url="wss://stream.binance.com:9443/ws/steembtc@depth@100ms", # url for connection to stream
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open
        )

        # local orderbook data management
        self.orderbook = {}  # creation of empty orderbook dict for first time
        # print('from orderbook',self.orderbook)
        self.updates = 0 # updates first time equal zero
        # print('from updates',self.updates)
        self.Mylist = []  #collection of messages in list
        self.timing = []  #collection of times values for messages in list
    # keep connection alive
    def run_forever(self, *args):
        self.ws.run_forever()

    # convert message to dict, print
    def on_message(self, ws, message):
        current_time = time.strftime('%M.%S', time.localtime())
        print('Current time mm:ss',float((current_time)))
        self.timing.append(current_time)
        print(self.timing)

        # seconds = time.time()
        # a = time.ctime(seconds)
        # print(a)
        # self.timing.append(a)
        # print(self.timing)

        self.Mylist.append(message)
        print(self.Mylist)
        print("Time difference in mm:ss",float(self.timing[-1]) - float(self.timing[0]))
        # data = loads(message)
        # len_of_my_list = len(self.Mylist)
        # h = self.Mylist
        # {x: ast.literal_eval(h) for x in range(len_of_my_list)}
        # print(x)
        # for i in h :
        #     ast.literal_eval(h)
        #     print(h)
        len_of_my_list = len(self.Mylist)
        print(len_of_my_list)
        h = self.Mylist # full list
        print('full_list',h)
        print(type(h))
        h0 = self.Mylist[0]
        print('h0',h0)
        h1 = self.Mylist[1]
        print('h2',h1)
        UPDID0 = ast.literal_eval(h0)  #convert Mylist[0] to dict
        UPDID1 = ast.literal_eval(h1)  # convert Mylist[1] to dict
        # print(type(UPDID))
        print('UPDID1',UPDID0['u'])
        print('UPDID2', UPDID1['u'])

        # h = self.Mylist[0]
        # UPDID = ast.literal_eval(h)  #convert Mylist to dict
        # # print(type(UPDID))
        # print('UPDID',UPDID)

        print("Time difference in mm:ss",float(self.timing[-1]) - float(self.timing[0]))
        # data = loads(message)

        # b = float(self.timing[-1]) - float(self.timing[0])
        # print('diff',b)
        # print('Output of Mylist[0]',h)
        # print('type of Mylist',type(h))
        # print(type(self.Mylist))
        # print('DICT',dict.fromkeys(self.Mylist[0]))
        # print('Hello',ast.literal_eval(h))


        # UPDID = ast.literal_eval(h)  #convert Mylist to dict
        # print(type(UPDID))
        # data = loads(h)
        # print('DATA',data)
        # print(type(data))

        if float(self.timing[-1]) - float(self.timing[0]) > 1:
            self.timing.clear()
            self.orderbook = self.get_snapshot()

    def on_error(self, *error):
        print(error)

        # run when websocket is closed

    def on_close(self, *args):
        print("### closed ###")

        # run when websocket is initialised

    def on_open(self, *args):
        print('Connected to Binance\n')

    def get_snapshot(self):
        r = requests.get('https://www.binance.com/api/v1/depth?symbol=STEEMBTC&limit=1000')
        print('From get_snapshot')
        return loads(r.content.decode())


if __name__ == "__main__":
    # create websocket client
    client = Client()
    # run forever
    client.run_forever()