import websocket
import requests
from json import loads
# import pdb

class Client():
    def __init__(self):
        # create websocket connection
        self.ws = websocket.WebSocketApp(
            url="wss://stream.binance.com:9443/ws/steembtc@depth@100ms",
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open
        )

        # local orderbook data management
        self.orderbook = {}
        # print('from orderbook',self.orderbook)
        self.updates = 0
        # print('from updates',self.updates)

    # keep connection alive
    def run_forever(self):
        self.ws.run_forever()

    # convert message from json to dict, print
    def on_message(self,ws, message):
        data = loads(message)
        # print(data)

        # check for orderbook, if empty retrieve fresh snapshot
        if len(self.orderbook) == 0:
            self.orderbook = self.get_snapshot()

        # get last Update Id from local orderbook
        lastUpdateId = self.orderbook['lastUpdateId']
        print('EEEEEEEE',type(self.orderbook))
        print("Local lastUpdateId is",lastUpdateId)

        # drop any updates that older than fresh snapshot
        if self.updates == 0:
            # print('data "u" is:', data['u'])
            # print('data is',data)
            if data['U'] <= lastUpdateId + 1 and data['u'] >= lastUpdateId + 1:
                print('Process this update')
                # print('dssaas',self.orderbook)
                # self.updates += 1
                self.orderbook['lastUpdateId'] = data['u']
            else:
                print('Discard update')

        # check if update still in sync state with orderbook
        elif data['U'] == lastUpdateId + 1:
            # print('process this update_1')
            self.orderbook['lastUpdateId'] = data['u']
            # print('from on message elif',self.orderbook)
        else:
            print('Out of sync, abort')
        client.process_updates(data)
    # catch errors
    def on_error(self, *error):
        print(error)

    # run when websocket is closed
    def on_close(self, close):
        print("### closed ###")

    # run when websocket updates is subscribed
    def on_open(self, *args):
        print('###Connected to Binance###\n')

    # Loop through all bid and ask updates, call manage_orderbook accordingly
    def process_updates(self, data):
        # print('From process_updates')
        for update in data['b']:
            # print('Hello from process_updates')
            self.manage_orderbook('bids', update)
            # print('From process updates',data)
        for update in data['a']:
            # print('From process updates 2')
            self.manage_orderbook('asks', update)
        print()

    # Update orderbook, differentiate between remove, update and new
    def manage_orderbook(self, side, update):
        # extract values
        # print('From manage_book')
        price, qty = update

        # loop through orderbook side
        for x in range(0, len(self.orderbook[side])):
            if price == self.orderbook[side][x][0]:
                # when qty is 0 remove from orderbook, else
                # update values
                if qty == 0:
                    del self.orderbook[side]
                    print(f'Removed {price} {qty}')
                    break
                else:
                    self.orderbook[side][x] = update
                    print(f'Updated: {price} {qty}')
                    break
            # if the price level is not in the orderbook,
            # insert price level, filter for qty 0
            elif price > self.orderbook[side][x][0]:
                if qty != 0:
                    self.orderbook[side].insert(x, update)
                    print(f'New price: {price} {qty}')
                    break
                else:
                    break
    # retrieve orderbook snapshot
    def get_snapshot(self):
        r = requests.get('https://www.binance.com/api/v1/depth?symbol=STEEMBTC&limit=1000')
        # print('From get_snapshot')
        return loads(r.content.decode())


if __name__ == "__main__":
    # create webscocket client
    client = Client()

    # run forever
    client.run_forever()